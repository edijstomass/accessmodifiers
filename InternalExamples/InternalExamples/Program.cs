﻿using System;
using InternalProjectTwo;
using AssemblyThree;

namespace InternalExamples
{
    class Program  // this is excecutable assembly and class is DLL assembly
    {
        static void Main(string[] args)
        {
            Mammal obj1 = new Mammal();     // internal member, from the same assembly
            obj1.id = 555;
            Console.WriteLine(obj1.id);

            Reptile obj2 = new Reptile();   // internal member, from other assembly
            obj2.number = 999;              // no access, cuz internal! 
            Console.WriteLine(obj2.number);

                                            // protected internal look in InternalProjectTwo/Reptile



        }
    }
}
