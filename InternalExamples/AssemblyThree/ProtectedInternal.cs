﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AssemblyThree
{
  public class ProtectedInternal    // protected internal type members can access in other assamblies, using inheritance
    {
        protected internal int sum = 0; 
    }
}
