﻿using System;
using AssemblyThree;

namespace InternalProjectTwo
{
   public class Reptile : ProtectedInternal     // must inherit, to access protected internal type member from other assembly
    {
        internal int number = 0;

        public void PrintMethod()
        {
            Reptile obj67 = new Reptile();
            obj67.sum = 55;
            //OR
            Reptile obj68 = new Reptile();
            base.sum = 99;


        }

    }
}
