﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProtectedExamples
{
    public class Employee
    {
        private string privateString = "This string is private";
        protected string protectedString = "This string is protected";  // can access only child class !!!! 
        private string name;
        private int id;



        protected int year { get; set; } // protected property // can access only when creating an instance
        
        public Employee()
        {
            
        }

        public Employee(string Name, int Id, string imp, int year)
        {
            name = Name;
            id = Id;
            protectedString = imp;
            this.year = year;

        }




        public override string ToString()
        {
            return $"Name: {this.name}, protected String: {this.protectedString}";
        }


    }
}
