﻿using System;

namespace ProtectedExamples
{
    class Program
    {
        static void Main(string[] args)
        {
            Costumer cost1 = new Costumer();
            cost1.Display();
            

            //cost1.protectedString = "Some text";      also not possible

            Employee emp1 = new Employee("Edijs", 55, "This is protected string777", 1993);
            //Console.WriteLine(emp1.year) no access
            // emp1.year = 1231231; no access 

            Console.WriteLine(emp1.ToString());
           


        }
    }
}
